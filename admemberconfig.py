# Example script uses Tim Golden's active_directory module
# Replaces all occurances of AD{ad_grp_name} with the members of the AD group

import re
import active_directory

# match AD{}
regex = "AD{([A-Za-z]\w+)}"

# usually we would read the config template from a file
template = """# Members in AD groups
Group admin has members AD{admin}"""

def adgrouplookup (matchobj):
    'Looks up the group in AD and returns the sAMAccountName comma seperated'
    grpusers= set()
    adgrp = active_directory.find_group(matchobj.group(1))
    if adgrp: # group found in AD
        for group, groups, users in adgrp.walk():
            grpusers.update(users)
        return ",".join(ad_item['sAMAccountName'] for ad_item in grpusers)
    else: # return matched string unchanged
        return matchobj.group()

# For our demo we will print the result but usually this would be saved as the config file
print(re.sub(regex,adgrouplookup,template))

# EoF: admemberconfig.py
