#!/usr/bin/env python
"In-browser Python code editor with code execution on server"

import bottle
import sys
import contextlib
import StringIO
import traceback

page="""<!DOCTYPE html>
<html lang="en">
<head>
<title>ACE Python</title>
</head>
<body>
  <h1>Editor</h1>
  <div id="container">
    <div id="resultcontainer" style="float: right; width: 40%;">
      <div id="result" style="margin: 10px; padding: 1em; min-height: 50px;">
	    Click <em>Run Code</em> to execute python code and see the result here
	  </div>
	</div>
    <div id="editorcontainer" style="float: right; width: 59%;">
      <div id="editorouter" style="margin: 10px; min-height: 50px;">
	    <div id="editor" style="height: 400px;">print("Hello World")</div>
      </div>
    </div>
  </div>
  <button id="runbutton" type="button">Run Code</button>
  <script src="https://cdn.jsdelivr.net/ace/1.2.3/min/ace.js" type="text/javascript" charset="utf-8"></script>
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" type="text/javascript" charset="utf-8"></script>
  <script>$(function() {
ace.config.set("packaged", true);
var editor = ace.edit("editor");
editor.setTheme("ace/theme/monokai");
editor.getSession().setMode("ace/mode/python");

$("#runbutton").click(function(){
  $.post("run", editor.getValue(), function(data) {
    $("#result").html(data);
  }, "text");
});
});</script>
</body>
</html>"""

@contextlib.contextmanager
def stdout_redirect(stream=None):
    old_stdout = sys.stdout
    if stream is None:
        stream = StringIO.StringIO()
    sys.stdout = stream
    try:
        yield stream
    finally:
        sys.stdout = old_stdout

def exec_code (codestr):
    'runs the code capturing the stdout'
    try:
        code = compile(codestr,"<string>","exec")
    except: # catch all compilation exceptions
        return "Compile time error\n" + "-" * 18 + "\n\n" + traceback.format_exc()

    with stdout_redirect() as output:
        try:
            exec(code,{},{})
        except:
            return "Runtime error\n" + "-" * 13 + "\n\n" + traceback.format_exc()

    return output.getvalue()

@bottle.route('/')
def mainpage ():
    'Displays the page'
    return page

@bottle.route('/run', method='POST')
def runcode ():
    'runs the code'
    return exec_code(bottle.request.body.read())

if __name__ == "__main__":
    bottle.run(host='localhost',port=8081,debug=True)

# EoF: pyedit.py
