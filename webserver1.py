import sys

CNTR = 0

def index (env):
    "Displays all the available pages"
    tablevars = [ '<tr><td><a href="%s">%s</a></td><td>%s</td></tr>\n' %
                  (x,filesdic[x].__name__,filesdic[x].__doc__) for x in filesdic
                  if x != "/"]
    tablevars.insert(0,'<html>\n<head><title>Index</title></head>\n<body><table>\n')
    tablevars.append('</table></body>\n</html>')
    return tablevars

def counter (env):
    "Displays a counter showing the number of times the page has been viewed"
    CNTR += 1
    return [ '<html><body><p>This page has been viewed %d times</p></body></html>' % (CNTR)]

def testver (env):
    "Displays the Python version"
    return [ '<html>\n<head><title>Index</title></head>'
             '<body>\n<h1>Python Version</h1>\n<p>',
             sys.version, '</p>\n</body>\n</html>' ]

def testenv (env):
    "Displays the web server environment variables"
    tablevars = [ '<tr><td>%s</td><td>%s</td></tr>\n' %
                  (x,env[x]) for x in env ]
    tablevars.insert(0,'<html>\n<head><title>Environment Variables</title></head>\n<body><table>\n')
    tablevars.append('</table></body>\n</html>')
    return tablevars

filesdic = { '/': index, '/index.html': index,    # index page
             '/counter.html': counter,            # simple counter
             '/test/version.html': testver,       # page in test
             '/test/environment.html': testenv }  # another test page
port = 8081

def webapp(environ, start_response):
    filename = environ.get('PATH_INFO', '').lower()
    if filename in filesdic:
        status = '200 OK' # HTTP Status
        headers = [('Content-type', 'text/html; charset=utf-8')] # HTTP Headers
        start_response(status, headers)
        return [x.encode('utf-8') for x in filesdic[filename](environ)]
    else:
        status = '404 Not Found' # HTTP Status
        headers = [('Content-type', 'text/plain; charset=utf-8')] # HTTP Headers
        start_response(status, headers)
        response = 'Unable to locate %s' % (filename)
        return [response.encode('utf-8')]

from wsgiref.simple_server import make_server
srv = make_server('localhost',port,webapp)
print "Webserver on port %s" % (port)
try:
    srv.serve_forever()
except KeyboardInterrupt:
    print "Server closed due to keyboard interrupt"
