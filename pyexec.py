#!/usr/bin/env python
"Demonstration of the ability to compile and execute Python code from a string"

import sys
import contextlib
import StringIO
import traceback

@contextlib.contextmanager
def stdout_redirect(stream=None):
    old_stdout = sys.stdout
    if stream is None:
        stream = StringIO.StringIO()
    sys.stdout = stream
    try:
        yield stream
    finally:
        sys.stdout = old_stdout

def exec_code (codestr):
    'runs the code capturing the stdout'
    try:
        code = compile(codestr,"<string>","exec")
    except: # catch all compilation exceptions
        return "Compile time error\n" + "-" * 18 + "\n\n" + traceback.format_exc()

    with stdout_redirect() as output:
        try:
            exec(code,{},{})
        except:
            return "Runtime error\n" + "-" * 13 + "\n\n" + traceback.format_exc()

    return output.getvalue()

print(exec_code("from + 2")) # invalid syntax will not compile
print(exec_code("x = 3\nx + 'a'\n")) # runtime error trying to add a character to a number
print(exec_code("x = 3\nprint(x+2)")) # error free
