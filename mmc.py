# Example script uses Tim Golden's active_directory module

import win32com.client
import pywintypes
import active_directory
import time

# Get a list of all items in AD with an operation System contain the word server
adobjs = active_directory.search("operatingSystem='*server*'")
servers = [ad.cn for ad in adobjs]
servers.sort()

# Use COM to create the MMC application and add the Remote Desktop snap-in
mmc = win32com.client.Dispatch("MMC20.Application")
mmc.Show()
try:
    rdp = mmc.Document.SnapIns.Add("Remote Desktops")
except pywintypes.com_error:
    print "Need to install & configure Remote Server Administration Tools"

# fiddle: fill the keyboard buffer to enter the names of the server
mmc.Document.ScopeNamespace.Expand(mmc.Document.RootNode)
wsh = win32com.client.Dispatch("WScript.Shell")
wsh.SendKeys("{DOWN}{RIGHT}")
for server in servers:
    print server
    wsh.SendKeys("+{F10}") # Context menu
    wsh.SendKeys("{DOWN}{ENTER}") # Select first menuitem - Add New Connection
    # Alt+A (to select computer name box) enter computer name then
    # Alt+N (untick console) and close dialog
    wsh.SendKeys("%%A%s%%N{ENTER}" % server)
    # The script can fill the keyboard full faster than MMC empties it
    # so we need a delay in the loop to slop the script down
    time.sleep(0.2) # adjust as necessary

# Swap MMC application to user control so it doesn't close when the script exits
mmc.UserControl = 1

# EoF: mmc.py
