"""simple web server to display the markdown documents
requires the following modules
bottle    (pip install bottle)
markdown  (pip install markdown)"""

import sys
import os
import bottle
import markdown
import getopt

# add modules directory to the import path
scriptpath = os.path.dirname(sys.argv[0])
if len(scriptpath) < 2:
    # no valid script path
    scriptpath = os.getcwd()

# global variables initiated to module classes
md = markdown.Markdown()

def page(contents,title=""):
    "standard html decoration"
    return """<!DOCTYPE html><html>
<head><title>%s</title></head>
<body>%s</body>
</html>""" % (title,contents)

@bottle.route('/')
def rootpage ():
    "Create a list of all documents available"
    files = os.listdir(scriptpath)
    doclist = '<ul>\n'
    for f in files:
        if f[-3:] == ".md":
            n = f[:-3]
            doclist += '<li><a href="/{0}">{0}</a></li>\n'.format(n)
    doclist += '</ul>\n'
    return page(doclist)

@bottle.route('/<name>')
def showdocs(name):
    "Show the given documentation"
    # check for a markdown document of that name and show if exists
    files = os.listdir(scriptpath)
    if name+'.md' in files:
        with open(os.path.join(scriptpath,name+".md")) as mdf:
            html = md.convert(mdf.read())
        return page(html)
    else:
        return page("<p>Page not found</p>")

if __name__ == "__main__":
    def printhelp(exit_code):
        print("""MarkUp: markdown document webserver
usage: markup.py [options]
-? --help
-h --host <host>    server name (defaults to localhost)
-p --port <port>    port (defaults to 8081)""")
        if exit_code is not None:
            sys.exit(exit_code)

    # defaults
    host='localhost'
    port=8081

    # import options if there are any
    if len(sys.argv):
        try:
            opts, args = getopt.getopt(sys.argv[1:],"?h:p:",
                                       ["help","host=","port="])
        except getopt.GetoptError:
            printhelp(1)
        for opt, arg in opts:
            if opt in ("-?", "--help"):
                printhelp(0)
            if opt in ("-h", "--host"):
                host=arg
            if opt in ("-p", "--port"):
                port=int(arg)

    bottle.run(host=host,port=port)
