# Example script showing how to access user registry hives 
# Opens up all hives in the inpath directory and prints out
# the user and a list of keys in Software

import win32security
import win32api
import win32con
import os
import os.path
from pywintypes import error as WindowsError

inpath = '<your profile root path>'

# function to enumerate subkeys and values of any given key
def EnumKey ( mainkey , path ):
    keys = list()
    values = list()
    try:
        hkey = win32api.RegOpenKeyEx(mainkey,path,0,win32con.KEY_READ)
    except:
        return ( keys , values )
    index = 0
    try:
        while True:
            keys.append(win32api.RegEnumKey(hkey,index))
            index += 1
    except WindowsError:
        pass
    index = 0
    try:
        while True:
            values.append(win32api.RegEnumValue(hkey,index))
            index += 1
    except WindowsError:
        pass
    if hkey:
        win32api.RegCloseKey(hkey)
    return ( keys , values )

# need privilage to access hives
# Thanks go to future_retro for the posting on grokbase for this
flags = win32security.TOKEN_ADJUST_PRIVILEGES | win32security.TOKEN_QUERY
htoken = win32security.OpenProcessToken(win32api.GetCurrentProcess(),flags)
loadid = win32security.LookupPrivilegeValue(None,'SeRestorePrivilege')
newprivlist = [(loadid, win32security.SE_PRIVILEGE_ENABLED)]
win32security.AdjustTokenPrivileges(htoken,0,newprivlist)

for inuser in os.listdir(inpath):
    count += 1
    profilepath = os.path.join(inpath,inuser)
    hive = os.path.join(profilepath,'ntuser.dat')
    if os.path.isdir(profilepath) and os.path.isfile(hive):
        try:
            win32api.RegLoadKey(win32con.HKEY_USERS,inuser,hive)
        except:
            continue
        print inuser,','.join(EnumKey(win32con.HKEY_USERS,inuser+"\\software")[0])
        win32api.RegUnLoadKey(win32con.HKEY_USERS,inuser)

# EoF: RegHiveList.py
