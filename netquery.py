# Uses Tim Golden's WMI and active_directory modules

import wmi
import active_directory

def _display_all_ ( compname , objlist ):
    for obj in objlist:
        print(compname+'\t'+'\t'.join([str(obj.__getattr__(x))
                                       for x in obj._getAttributeNames()
                                       if not hasattr(obj.__getattr__(x),'__call__')]))

def adwmiquery ( wql = "SELECT Caption,VolumeName,Size,Freespace FROM win32_logicaldisk WHERE DriveType=3",
                 search = "operatingSystem='*server*'" ,
                 filterfn = _display_all_ ):
    """Finds all of the computers maching the given search string from AD and finds
    the drive space of that computer. This is passed to given filterfn method to
    process as required. By default the search is all servers and the filter
    prints all the details"""

    adobjs = active_directory.search(search,"objectClass='computer'")
    for obj in adobjs:
        try:
            query = wmi.WMI(obj.cn)
            filterfn(obj.cn,query.query(wql))
        except:
            continue # computer not reachable or query error

if __name__ == "__main__":
    import sys
    if len(sys.argv) < 2:
        adwmiquery()
    else:
        adwmiquery(" ".join(sys.argv[1:]))

# EoF: netquery.py
