import re
import datetime
import platform

# usually we would read the template from a file
regex = "{{([A-Za-z]\w+)}}"
template = """This file was created at {{time}} on {{Date}}.
The operating system is {{OS}} version {{ver}}.
This {{variable}} is not matched so is unafffected."""

def mydate ():
    'Returns the current date'
    return datetime.datetime.now().strftime('%d %b %Y')

def mytime ():
    'Returns the current time'
    return datetime.datetime.now().strftime('%H:%M')

# dictionary with the allowed variables and the functions they use
myvals = { 'date': mydate, 'time': mytime,
           'os': platform.system, 'ver': platform.release }

def varsub (matchobj):
    'Changes the matched item to the variable'
    fn = matchobj.group(1).lower()
    if fn in myvals:
        return myvals[fn]()
    else:
        return matchobj.group()

# with everything set up, the actual code is a single line
print(re.sub(regex,varsub,template))

# EoF: subtemplate.py
