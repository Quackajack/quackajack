from pdfminer.pdfinterp import PDFResourceManager, PDFPageInterpreter
from pdfminer.converter import TextConverter
from pdfminer.layout import LAParams
from pdfminer.pdfpage import PDFPage
import io

def pdf_to_text(pdffile, strio):
    pdfrmgr = PDFResourceManager()
    laparam = LAParams()
    process = TextConverter(pdfrmgr, strio, laparams=laparam)

    with open(pdffile, 'rb') as fp:
        interpreter = PDFPageInterpreter(pdfrmgr, process)
        for page in PDFPage.get_pages(fp):
            interpreter.process_page(page)

    process.close()

with io.StringIO() as pdftext:
    pdf_to_text(r'C:\path\to\file.pdf',pdftext)
    print(pdftext.getvalue().encode())