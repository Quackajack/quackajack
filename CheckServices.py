# Example script using Tim Golden's WMI module
# Interrogates servers SERVER01 to SERVER29 for stopped services
# Any services that are listed in services tuple and have stopped are started

import wmi

services = ( "RpcSs" , "Spooler" , "wuauserv" )

for index in range(1,29):
    compwmi = wmi.WMI("SERVER%02d" % index)
    for s in compwmi.Win32_Service():
        if s.Caption in services and s.State == 'Stopped':
            print index,s.Caption,s.State
            result, = s.StartService()

# EoF: CheckServices.py
