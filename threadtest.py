import threading
import time

class simplesleep(threading.Thread):
    def __init__(self, threadname, sleeptime = 15):
        super(simplesleep,self).__init__()
        self.tloop  = False
        self.tname  = threadname
        self.sleept = sleeptime
        self.status = "Thread %s initialised" % self.tname

    def run(self):
        self.tloop = True
        while self.tloop:
            self.status = "Thread %s running" % self.tname
            print(self.status) # show status
            time.sleep(self.sleept)

    def stop(self):
        self.tloop = False
        self.status = "Thread %s stopped" % self.tname

if __name__ == "__main__":
    names = ("Tom","Dick","Harry","Will","Sally")

    print("Creating and starting the threads")
    threads = []
    for t in range(len(names)):
        threads.append(simplesleep(names[t],5+t))
        print(threads[t].status)
        threads[t].start()

    time.sleep(5*60) # wait 5 minutes

    print("Stopping the threads")
    for t in range(len(names)):
        print(threads[t].status)
        threads[t].stop()
        print(threads[t].status)
    
