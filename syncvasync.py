import grequests
import requests
import time

urls = ('https://www.google.com', 'https://www.android.com',
        'https://www.bing.com', 'https://www.microsoft.com/en-us/',
        'https://www.yahoo.com', 'https://www.tumblr.com/',
        'https://duckduckgo.com', 'http://www.ask.com',
        'https://www.facebook.com', 'https://www.twitter.com')

astart = time.time()
geturls = (grequests.get(u) for u in urls)
async_resp = grequests.map(geturls)
aend = (time.time() - astart) * 1000

start = time.time()
sync_resp = map(requests.get,urls)
end = (time.time() - start) * 1000

def printres (name, millis, responses):
    print ("%s gets took %d milliseconds" % (name,millis))
    for resp in responses:
        print ("%s took %d ms" % (resp.url,resp.elapsed.seconds*1000 + resp.elapsed.microseconds/1000))

printres("Asynchronous",aend,async_resp)
printres("Synchronous",end,sync_resp)
