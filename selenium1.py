from selenium import webdriver

# xpath for an anchor tag (<a>) which contains my blog hostname
xpath = ".//a[contains(@href, '%s')]" % "quackajack.wordpress.com"

# Repeat on Firefox, Chrome and Edge browsers

driver = webdriver.Firefox()
# driver = webdriver.Chrome()
# driver = webdriver.Edge()]
# driver = webdriver.Ie()
# driver = webdriver.opera.webdriver.OperaDriver()

driver.implicitly_wait(10)
driver.get("http://www.google.co.uk")

# search for my blog
search_field = driver.find_element_by_name("q")
search_field.clear()
search_field.send_keys("quackajack syspython")
search_field.submit()

# report back
print("Browser : %s " % driver.name)
print("Page title : %s" % driver.title)
if len(driver.find_elements_by_xpath(xpath)):
    print("Blog on first page")
else:
    print("Need to do some SEO")

driver.quit()