import sys
import win32com.client
import pywintypes

def showResults ( lrs ):
    head = ""
    sep = ""
    for col in range(lrs.getColumnCount()):
        if (head):
            head += '\t'
            sep += '\t'
        head += lrs.getColumnName(col)
        sep += '-' * len(lrs.getColumnName(col))
    print head
    print sep

    while not lrs.atEnd():
        lr = lrs.getRecord()
        line = ""
        for col in range(lrs.getColumnCount()):
            if (line):
                line += '\t'
            line += str(lr.getValue(col)) + "\t"
        print line
        lrs.moveNext()
    
lp = win32com.client.Dispatch("MSUtil.LogQuery")
sql = "Dummy"
while sql:
    print "Enter query (press enter to exit)"
    sql = sys.stdin.readline().strip()
    if (sql):
        try:
            showResults(lp.Execute(sql))
        except pywintypes.com_error:
            print "Invalid SQL command"

# EoF: InteractiveLogPartser.pywintypes
